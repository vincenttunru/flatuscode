# Change Log

The following changes have been implemented but not released yet:

## [Unreleased]

The following sections document changes that have been released already:

## [1.3.1] - 2023-11-19

### Changed

- Updated dependencies, and republished just to make sure I'm still able to push
  updates.

## [1.3.0] - 2022-03-31

### New features

- Added a new command to the command palette: "This is a smelly business". It
  enables farts just for files with the active file's language.

### Bugs fixed

- Language-specific configuration didn't work.

## [1.2.0] - 2022-03-31

### New features

- You can now enable Whoopie Cusion Keyboard for specific languages only.

## [1.1.1] - 2020-11-15

### Bugs fixed

- Apparently the extension did not work on Windows. Switching to a different sound playing library
  hopefully resolves that.

## [1.1.0] - 2020-11-15

### New features

- I added a couple additional farts, because why not.

## [1.0.0] - 2020-11-15

The "no idea why I'm doing SemVer - it's not like you're not getting automatic updates" release.
But v1 just sounds nice, I guess.

### New features

- It's now possible to enable and disable the sound effects through the settings, or via the Command
  Palette ("Let it rip!" and "Hold them in", respectively). I don't know why you'd want to disable
  it, but there you go.

## [0.0.1] - 2020-11-14

- Initial release. This is a silent one.
