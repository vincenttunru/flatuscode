# Whoopee cushion keyboard

Did you ever get that feeling like you're an imposter, programming and earning
a living like a real adult would do, but fully expecting at any moment to be
found out?

Well, stop hiding! Let it be known that, while you may _look_ like a fully-grown
adult, behind that mask hides an inner five-year-old that barely knows how to
make sense of the world.

I bring you: Whoopee cushion keyboard! Farts on every keypress!

To all the five-year-old programmers out there: I got you.

# How to use

After installing the extension, just start typing!

In case you want to pretend to be a normal adult again, you can disable Whoopee cushion keyboard
in the settings, or through the Command Palette: search for **Hold them in**. Once you're ready to
expose your True You again, search the Command Palette for **Let it rip!**
