// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';
import { resolve } from 'path';
import { readdir } from 'fs';
import sound = require('sound-play');
import getPlayer = require('play-sound');

let documentChangeListenerDisposer: vscode.Disposable | null = null;
let fartPaths: string[] = [];

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {
	const player = getPlayer();
	const fartDir = resolve(__dirname, "..", "assets");
	readdir(fartDir, (err, files) => {
		fartPaths = files.filter(fileName => fileName.endsWith('.mp3'));
	});

	function play(path: string) {
		if (process.platform === "win32") {
			return sound.play(path);
		}
		return player.play(path);
	}

	documentChangeListenerDisposer = vscode.workspace.onDidChangeTextDocument((e) => {
		const config = vscode.workspace.getConfiguration("flatuscode", { languageId: e.document.languageId });
		const enabled = config.get<boolean>("enable");
		if (!enabled) {
			return;
		}
		if (fartPaths.length === 0) {
			console.log('Still initialising farts...');
			return;
		}
		const fartPath = resolve(fartDir, pickRandom(fartPaths));
		play(fartPath);
	});

	// The command has been defined in the package.json file
	// Now provide the implementation of the command with registerCommand
	// The commandId parameter must match the command field in package.json
	let disposableEnableCommand = vscode.commands.registerCommand('flatuscode.enable', () => {
		// The code you place here will be executed every time your command is executed

		const config = vscode.workspace.getConfiguration("flatuscode");
		config.update("enable", true, true);
	});
	let disposableEnableCurrentLanguageCommand = vscode.commands.registerCommand('flatuscode.enableCurrentLanguage', () => {
		// The code you place here will be executed every time your command is executed

		const currentLanguage = vscode.window.activeTextEditor?.document.languageId;
		if (!currentLanguage) {
			return;
		}
		const config = vscode.workspace.getConfiguration("flatuscode", { languageId: currentLanguage });
		config.update("enable", true, true, true);
	});
	let disposableDisableCommand = vscode.commands.registerCommand('flatuscode.disable', () => {
		// The code you place here will be executed every time your command is executed

		const config = vscode.workspace.getConfiguration("flatuscode");
		config.update("enable", false, true);
		const currentLanguage = vscode.window.activeTextEditor?.document.languageId;
		if (currentLanguage) {
			const currentLanguageConfig = vscode.workspace.getConfiguration("flatuscode", { languageId: currentLanguage });
			currentLanguageConfig.update("enable", false, true);
		}
	});
}

// this method is called when your extension is deactivated
export function deactivate() {
	if (documentChangeListenerDisposer) {
		documentChangeListenerDisposer.dispose();
		documentChangeListenerDisposer = null;
	}
}

function pickRandom<X>(items: X[]): X {
	return items[Math.floor(Math.random() * items.length)];
}
