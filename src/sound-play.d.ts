declare module "sound-play" {
  import { ChildProcess } from "child_process";

  export function play(path: string): Promise<ChildProcess>;
}
