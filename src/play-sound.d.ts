declare module "play-sound" {
  import { ChildProcess } from "child_process";

  interface Player {
    play(path: string, onError?: (error: null | (Error & { killed: boolean })) => void): ChildProcess;
  }

  function getPlayer(): Player;

  export = getPlayer;
}
